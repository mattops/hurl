{-# LANGUAGE OverloadedStrings #-}
module Main where

-- Standard Library
import Data.Version (showVersion)
import Data.Semigroup ((<>))
-- Third Party
import qualified Data.CaseInsensitive as CI
import qualified Data.Map as Map
import Data.Text.Encoding
import Data.Text (Text)
import qualified Data.Text as T
import qualified Data.Text.IO as TIO
import Data.ByteString.Lazy.Char8 (ByteString)
import Data.ByteString.Char8 (ByteString)
import qualified Data.ByteString.Lazy.Char8 as BS
import Control.Lens
import Network.HTTP.Types
import qualified Network.Wreq as W
import Options.Applicative
-- Local
import Paths_hurl (version)

data Opts = Opts
    { optMethod :: !String
    , optWriteFile :: !Bool
    , optHeaders :: !Bool
    , optUrl :: !String
    }

getFilename :: String -> FilePath
getFilename url = reverse $ takeWhile (/= '/') (reverse url)

getHeaders :: ResponseHeaders -> [Text]
getHeaders = map (\(name,value) -> decodeUtf8 (CI.original name <> " = " <> value))

main :: IO ()
main = do
    opts <- execParser optsParser
    case optMethod opts of
      "GET" -> do
          let url = optUrl opts
          resp <- W.get url
          if optWriteFile opts
             then writeFile (getFilename url) (BS.unpack $ resp ^. W.responseBody)
          else if optHeaders opts
               then mapM_ TIO.putStrLn (getHeaders $ resp ^. W.responseHeaders)
          else BS.putStr $ resp ^. W.responseBody
      _     -> putStrLn $ "unknown method " <> optMethod opts
  where
    optsParser     = info
        (helper <*> versionOption <*> programOptions)
        (fullDesc <> progDesc "hurl" <> header "hurl - a clone of curl")
    versionOption  =
        infoOption (showVersion version) (long "version" <> help "Show version")
    programOptions =
        Opts <$>
            strOption
                (short 'X' <> metavar "METHOD" <> value "GET" <>
                  help "The HTTP method to use") <*>
            switch
                (short 'O' <> help "Download to file") <*>
            switch
                (short 'I' <> help "Show response headers only") <*>
            strArgument
                (metavar "URL" <> help "URL of file to download")
